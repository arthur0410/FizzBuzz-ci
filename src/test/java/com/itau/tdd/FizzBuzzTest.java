package com.itau.tdd;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FizzBuzzTest {	

	@Test
	public void testeFizzBuzz100() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.nova_funcao(100);
		assertEquals("1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16 17 Fizz 19 Buzz Fizz 22 23 Fizz Buzz 26 Fizz 28 29 FizzBuzz 31 32 Fizz 34 Buzz Fizz 37 38 Fizz Buzz 41 Fizz 43 44 FizzBuzz 46 47 Fizz 49 Buzz Fizz 52 53 Fizz Buzz 56 Fizz 58 59 FizzBuzz 61 62 Fizz 64 Buzz Fizz 67 68 Fizz Buzz 71 Fizz 73 74 FizzBuzz 76 77 Fizz 79 Buzz Fizz 82 83 Fizz Buzz 86 Fizz 88 89 FizzBuzz 91 92 Fizz 94 Buzz Fizz 97 98 Fizz Buzz", retorno);
	}
	
	@Test
	public void testeFizzBuzz15() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.nova_funcao(15);
		assertEquals("1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz", retorno);
	}
	
	@Test
	public void testeFizzBuzz1() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.nova_funcao(1);
		assertEquals("1", retorno);
	}
	
	@Test
	public void testeFizzBuzz0() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.nova_funcao(0);
		assertEquals("", retorno);
	}
	
	@Test
	public void testeFizzBuzzNegativo() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.nova_funcao(-1);
		assertEquals("", retorno);
	}
	
	@Test
	public void testeBuzzValida5() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(5);
		assertEquals("Buzz", retorno);	
	}
	@Test
	public void testBuzzValida3() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(3);
		assertEquals("Fizz", retorno);	
	}
	
	@Test
	public void testBuzzValida3e5() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(15);
		assertEquals("FizzBuzz", retorno);	
	}

	@Test
	public void testBuzzValidaDiferente() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(2);
		assertEquals("2", retorno);	
	}

	@Test
	public void testBuzzValida6() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(6);
		assertEquals("Fizz", retorno);	
	}

	@Test
	public void testBuzzValida10() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(10);
		assertEquals("Buzz", retorno);	
	}
		

	@Test
	public void testBuzzValida30() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(30);
		assertEquals("FizzBuzz", retorno);	
	}
	
	@Test
	public void testBuzzValidaDiferente4() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(4);
		assertEquals("4", retorno);	
	}
}
